# -*- coding: utf-8 -*-
from odoo import fields, models


class PurchaseOrderApprover(models.Model):
    _inherit = "purchase.order.approver"

    req_id = fields.Many2one(
        comodel_name='purchase.requisition', string='Order', ondelete='cascade')

    order_id = fields.Many2one(
        required=False
    )
