# -*- coding: utf-8 -*-
from odoo import fields, models


class CatalogRoles(models.Model):
    _inherit = "catalog.roles"

    is_requisition = fields.Boolean(string="is for requisition?", )
