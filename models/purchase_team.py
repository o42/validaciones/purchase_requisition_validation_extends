# -*- coding: utf-8 -*-
from odoo import api, fields, models


class PurchaseTeam(models.Model):
    _inherit = "purchase.team"

    is_requisition = fields.Boolean(string="is requisition order?", )

    @api.onchange("is_requisition")
    def onchange_is_requisition(self):
        if self.is_requisition:
            domain = self.env["catalog.roles"].search([
                ("is_requisition", "=", True),
                ("active", "=", True)
            ]).mapped("id")
            self.domain_role_ids = [(4, id, 0) for id in domain]
        else:
            self.onchange_type_fill_domain_role_ids()
