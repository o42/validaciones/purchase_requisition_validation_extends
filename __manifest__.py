# -*- coding: utf-8 -*-
{
    'name': 'Requisition Validaciones extends',
    'version': '1.0.0',
    'summary': """
    Agrega validaciones en los purchase.requisition (Acuerdo de compra)
    """,
    'category': 'Purchases',
    'author': 'Odoo',
    'support': 'Odoo',
    'website': 'https://odoo.com',
    'description':
        """
Requisition Order Approval Cycle
=============================
This module helps to create multiple custom, flexible and dynamic approval route
for requisition orders based on purchase team settings.

Key Features:

 * This module is a extension for purchase_approval_route
        """,
    'data': [
        'views/purchase_requisition_views.xml',
        'views/purchase_team_views.xml',
        'views/catalog_roles_views.xml',
    ],
    'depends': ['purchase_requisition', 'extends_purchase_approval_route'],
    'installable': True,
    'auto_install': True,
    'application': False,
}
